package toDoTesting.test;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ToDoACStepDefinition {
	
	
	protected WebDriver driver;
	
	 @Before
	    public void setup() {
	        driver = new FirefoxDriver();
	}
		
	@Given("^I open ToDo$")
	public void I_open_ToDo() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://qa-test.avenuecode.com/users/sign_in");
	}
	
	@When("^I enter \"([^\"]*)\" in email textbox$")
	public void I_enter_in_email_textbox(String additionTerms) {
		WebElement emailTextBox = driver.findElement(By.id("user_email"));
		emailTextBox.sendKeys(additionTerms);

	}
	
	@When("^I enter \"([^\"]*)\" in password textbox$")
	public void I_enter_in_password_textbox(String additionTerms) {
		WebElement passwordTextBox = driver.findElement(By.id("user_password"));
		passwordTextBox.sendKeys(additionTerms);
					
		//Click on Sing in
		WebElement singinButton = driver.findElement(By.className("btn btn-primary"));
		singinButton.click();
	}
	
	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String loggedMessage) {
		WebElement logged = driver.findElement(By.className("alert alert-info"));
		String result = logged.getText();
				
		Assert.assertEquals(result, loggedMessage);
		
		driver.close();
	}
	
	 @After
	    public void closeBrowser() {
	        driver.quit();
	 }

}


