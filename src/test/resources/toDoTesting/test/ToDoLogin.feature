Feature: Log into the ToDo Rails and Angular Application
   In order to verify my ToDo List
   As a user of ToDo Rails and Angular Application
   I should be able log into the aplication

   Scenario: Login
   Given I open ToDo
   When I enter "ericksqueiroz@gmail.com" in email textbox
   When I enter "queirozerick123ac" in password textbox
   Then I should see "Signed in successfully."